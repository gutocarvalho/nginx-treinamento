include nginx

nginx::vhost { 'brasilia.puppet.com':
  vhostdir => '/srv/brasilia',
  port     => '81',
}

nginx::vhost { 'riodejaneiro.puppet.com':
  vhostdir => '/srv/rj',
  port     => '82',
}

nginx::vhost { 'saopaulo.puppet.com':
  vhostdir => '/srv/sp',
  port     => '83',
}

nginx::vhost { 'campogrande.puppet.com':
  vhostdir => '/srv/cgr',
  port     => '84',
}
