class nginx::params {
  case $::osfamily {
    'redhat','debian': {
      $pkgname = 'nginx'
      $owner = 'root'
      $group = 'root'
      $mode  = '0644'
      $docroot = '/var/www'
      $confdir = '/etc/nginx'
      $blockdir = '/etc/nginx/conf.d'
      $logdir = '/var/log/nginx'
      $servicename = 'nginx'
    }
    'windows': {
      $pkgname = 'nginx-service'
      $owner = 'Administrator'
      $group = 'Administrators'
      $mode  = '0660'
      $docroot = 'C:/ProgramData/nginx/html'
      $confdir = 'C:/ProgramData/nginx/conf'
      $blockdir = 'C:/ProgramData/nginx/conf.d'
      $logdir = 'C:/ProgramData/nginx/logs'
      $servicename = 'nginx'
    }
    default: {
        fail("Operating system ${::operatingsystem} is not supported.")
    }
  }
 # tratando usuario que vai rodar o serviço

  $username = $::osfamily ? {
    'debian'  => 'www-data',
    'windows' => 'nobody',
    default   => 'nginx',
  }

# tratando provider de pacotes para windows

  if $::kernel == 'windows' {
    Package {
      provider => 'chocolatey'
    }
  }

  # definindo valores padrao para recurso file

  File {
    owner => $owner,
    group => $group,
    mode  => $mode,
  }
}
