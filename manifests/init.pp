class nginx (
  $pkgname     = $nginx::params::pkgname,
  $owner       = $nginx::params::owner,
  $group       = $nginx::params::group,
  $mode        = $nginx::params::mode,
  $docroot     = $nginx::params::docroot,
  $confdir     = $nginx::params::confdir,
  $blockdir    = $nginx::params::blockdir,
  $logdir      = $nginx::params::logdir,
  $servicename = $nginx::params::servicename,
  $username    = $nginx::params::username,
  ) inherits nginx::params {

  package { $pkgname:
    ensure => present,
  }

  file { "${confdir}/nginx.conf":
    ensure  => file,
    content => template('nginx/nginx.conf.erb'),
    notify  => Service[$servicename],
  }

  nginx::vhost { 'default':
    port     => '80',
    vhostdir => $docroot,
  }

  service { $servicename:
    ensure => running,
    enable => true,
  }

}
