define nginx::vhost(
  $vhostdir,
  $port = '80',
  $servername = $title,
  ) {

  File {
    owner => $nginx::owner,
    group => $nginx::group,
    mode  => $nginx::mode,
  }

  # criando uma entrada no hosts do sistema operacional

  host { $servername:
    ensure => present,
    ip     => '127.0.0.1',
  }

  # criando diretorio do site

  file { $vhostdir:
    ensure => directory,
  }

  # criando index do site

  file { "${vhostdir}/index.html":
    ensure  => file,
    content => template('nginx/index.html.erb'),
  }

  # criando vhost do nginx

  file { "${nginx::blockdir}/${servername}.conf":
    ensure  => file,
    content => template('nginx/vhost.conf.erb'),
    notify  => Service[$nginx::servicename],
  }

}
